package com.popbee;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.hkm.advancedtoolbar.V5.BeastBar;
import com.hkm.slidingmenulib.layoutdesigns.singleDetailPost;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;
import com.popbee.pages.detail.commentbox;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrListener;

/**
 * Created by hesk on 23/7/15.
 */
public class Comment extends singleDetailPost<commentbox> implements SlidrListener {
    private BeastBar actionToolBar;
    private commentbox mcomme;

    public static Bundle fbBun(final @StringRes int title, final String id_comment) {
        final Bundle n = new Bundle();
        n.putInt(Method, REQUEST_METHOD_FULL_URL);
        n.putString(requestURL, id_comment);
        return n;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //  Slidr.attach(this, PBUtil.getSlidrConfig(this, this));
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void loadPageWithFullURL(String url) {
        Log.d("loadPage_", url);
    }


    @Override
    protected void loadPageWithPID(long pid) {
        Log.d("loadPage_", pid + "");
    }

    /**
     * setting the first initial fragment at the beginning
     *
     * @return generic type fragment
     * @throws Exception the exception for the wrongs
     */
    @Override
    protected commentbox getInitFragment() throws Exception {
        if (url == null) {
            startIntentArgument();
        }
        return mcomme = commentbox.B(commentbox.fbIntent(R.string.comment_area, url));
    }

    @Override
    protected void onMenuItemSelected(@IdRes int Id) {

    }


    /**
     * the location to setup and configure the toolbar widget under AppCompat V7
     *
     * @param v7 Toolbar object
     */
    @Override
    protected void configToolBar(Toolbar v7) {
        BeastBar.Builder bb = new BeastBar.Builder();
        bb.back(R.mipmap.ic_action_arrow_left);
        bb.background(R.drawable.actionbar_bg_white_greyline);
        bb.setToolBarTitleSize(R.dimen.toolbar_title);
        bb.setToolBarTitleColor(R.color.main_background);
        // bb.setFontFace(this, "FrancoisOne.ttf");
        bb.defaultTitle(this.getString(R.string.comment_area));
        actionToolBar = BeastBar.withToolbar(this, v7, bb);
        actionToolBar.setBackIconFunc(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
        Slidr.attach(this, PBUtil.getSlidrConfig(getApplication(), this));
    }

    @Override
    public void onSlideStateChanged(int i) {

    }

    @Override
    public void onSlideChange(float v) {

    }

    @Override
    public void onSlideOpened() {

    }

    @Override
    public void onSlideClosed() {

    }

    @Override
    protected boolean hasStatusBarPadding() {
        return false;
    }

    /**
     * setting the default main activity layout ID and this is normally had presented in the library and no need change unless there is a customization need for different layout ID
     *
     * @return resource id
     */
    @Override
    protected int getDefaultMainActivityLayoutId() {
        return R.layout.custom_single;
    }
}
