package com.popbee.pages.detail;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.hkm.advancedtoolbar.socialbar.combar;
import com.hkm.slider.SliderLayout;
import com.hkm.slider.SliderTypes.BaseSliderView;
import com.hkm.slider.SliderTypes.DefaultSliderView;
import com.hkm.slider.TransformerL;
import com.popbee.R;
import com.popbee.life.Ad;
import com.popbee.life.PBUtil;

import java.util.List;

/**
 * Created by hesk on 7/5/2015.
 */
public class detail extends detailbse {
    public interface loadData {
        void onLoad();

        void onShareClick();

        void onCommentClick();
    }

    private loadData bloadlistenr;
    private View content_view;

    @Override
    public void onViewCreated(View v, Bundle b) {
        initBinding(v);
        if (bloadlistenr != null)
            bloadlistenr.onLoad();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void presetGallery(final SliderLayout moduleSlider) {
        moduleSlider.stopAutoCycle();
        moduleSlider.setPresetTransformer(TransformerL.Default);
        //moduleSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        // moduleSlider.setCustomAnimation(new DescriptionAnimation());
        //moduleSlider.getPagerIndicator().setDefaultIndicatorColor(R.color.indicator_0, R.color.indicator_1);

        moduleSlider.setSliderTransformDuration(500, new LinearOutSlowInInterpolator());
        moduleSlider.setOffscreenPageLimit(3);
        moduleSlider.setNumLayout(new NumZero(getActivity()));
    }

    public void setLoader(final loadData b) {
        bloadlistenr = b;
    }

    @Override
    protected int singleArticle() {
        return R.layout.article_single;
    }


    @Override
    protected void extraBinding(final View fm) {
        ImageButton t = (ImageButton) fm.findViewById(R.id.share_button_ssn);
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bloadlistenr.onShareClick();
            }
        });
        ImageButton g = (ImageButton) fm.findViewById(R.id.open_comment);
        g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bloadlistenr.onCommentClick();
            }
        });
        content_view = fm;
        Ad.newBannerView((RelativeLayout) content_view.findViewById(R.id.admob_banner));
    }

    public void complete() {
        completeloading();
    }

    public void setup_content(final String title, final String cat, final String author, final String contentblock) {
        setup_infox(title, cat, author, contentblock);
    }

    public void setup_video(final String embeding_code) {
        video_webview(embeding_code);
    }

    public void setup_single_im(final String url) {
        setup_static_image(url);
    }

    public void setup_gallery(List<String> list) {
        try {
            if (getSlid().getPagerIndicator().getChildCount() > 0) {
                getSlid().removeAllSliders();
            }
            for (String entry : list) {
                DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
                textSliderView
                        .image(entry)
                        .setScaleType(BaseSliderView.ScaleType.CenterInside);
                //.setOnSliderClickListener(this);
                //add your extra information
                textSliderView.enableSaveImageByLongClick(getChildFragmentManager());
                getSlid().addSlider(textSliderView);
            }
            getSlid().presentation(SliderLayout.PresentationConfig.Smart);
            PBUtil.startToReveal(getSlidFrame(), 2000);
        } catch (NullPointerException e) {
            Log.d(TAG, e.getMessage());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public void setShareContentBar(String title, String except, String link) {
        combar mCombo = (combar) content_view.findViewById(R.id.social_bar_combar);
        mCombo.connectAlert(getChildFragmentManager())
                .setShareContent(title, except, link);
    }

    public void closeVideo() {
        hidevideo();
    }

    public void closeSlider() {
        if (PBUtil.hideSlider(getSlidFrame())) {
            if (getSlid().getPagerIndicator().getChildCount() > 0) {
                getSlid().removeAllSliders();
            }
        }
    }
}
