package com.popbee.pages.featureList;


import android.os.Bundle;

import com.popbee.R;

/**
 * Created by hesk on 19/10/15.
 */
public class SearchTemplate extends CommonListTemplate {
    public static SearchTemplate B(final Bundle b) {
        final SearchTemplate t = new SearchTemplate();
        t.setArguments(b);
        return t;
    }

    @Override
    protected int setEmptyListViewId() {
        return R.layout.emptyview;
    }
}
