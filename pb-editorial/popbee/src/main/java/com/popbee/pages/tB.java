package com.popbee.pages;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.popbee.R;
import com.popbee.pages.featureList.TrendinListTemplate;
import com.popbee.pages.featureList.basicfeed;
import com.popbee.pages.featureList.CommonListTemplate;

/**
 * binding,
 * Created by hesk on 2/7/15.
 */
public enum tB {
    latest(R.string.tabpage1, CommonListTemplate.class) {
        @Override
        public Bundle bundle() {
            return CommonListTemplate.con_latest(getTitle());
        }
    },
    trending(R.string.tabpage2, TrendinListTemplate.class) {
        @Override
        public Bundle bundle() {
            return CommonListTemplate.con_cate("trending");
        }
    };

    private final Class<? extends basicfeed> clazz;
    private final int name_title;

    tB(final @StringRes int name_title, final Class<? extends basicfeed> clazz) {
        this.clazz = clazz;
        this.name_title = name_title;
    }

    protected int getTitle() {
        return name_title;
    }

    public abstract Bundle bundle();

    public Class<? extends Fragment> getClazz() {
        return clazz;
    }

    public int getStringId() {
        return name_title;
    }
}
