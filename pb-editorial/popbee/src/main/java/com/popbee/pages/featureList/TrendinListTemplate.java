package com.popbee.pages.featureList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.hypebeast.sdk.Util.Connectivity;
import com.hypebeast.sdk.Util.PrettyTime;
import com.hypebeast.sdk.api.exception.ApiException;
import com.hypebeast.sdk.api.model.popbees.pbpost;
import com.hypebeast.sdk.api.resources.pb.pbPost;
import com.hypebeast.sdk.clients.PBEditorialClient;
import com.popbee.life.Ad;
import com.popbee.life.Config;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;

import java.text.ParseException;
import java.util.List;

import retrofit.client.Response;

/**
 * Created by hesk on 20/7/15.
 */
public class TrendinListTemplate extends trendbase<pbpost> {

    public static TrendinListTemplate B(final Bundle b) {
        final TrendinListTemplate t = new TrendinListTemplate();
        t.setArguments(b);
        return t;
    }


    @Override
    protected void binddata(binder holder, final pbpost data, int position) {
        try {
            final boolean f = Connectivity.isConnectedFast(getActivity());
            final String i = f ? data.FImage.image.msizes.med.imageURL : data.FImage.image.msizes.thumb.imageURL;
            picasso.load(i).into(holder.big_image_single);
            holder.tvtime.setText(PrettyTime.getMoment(PBEditorialClient.DATE_FORMAT, data.mdate));
            holder.rank.setText((position + 1) + "");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * trigger point to the new page.
         */
        holder.click_detection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItem(data.id);
            }
        });
        holder.tvtitle.setText(data.title);
        PBUtil.stylePBCategories(getActivity(), holder.cate_name, data.terms.cates.get(0).name);
    }


    protected pbPost interfacerequest;
    protected LifeCycleApp app;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (LifeCycleApp) activity.getApplication();
        interfacerequest = app.popbeeInterface();
    }

    protected void createHypbridAdapter(List<pbpost> source) {
        final trendbase.cateadapter noad = new trendbase.cateadapter(source);
        final trendbase.admob lol = new trendbase.admob(Ad.newAdViewTrending(getActivity()), false, Ad.AD_INTERVAL, source, lsi);
        sw = new patchHyprbid(listview_layout, noad, lol);
    }


    /**
     * Successful HTTP response.
     *
     * @param list     the normnal return
     * @param response the response in object
     */
    @Override
    public void success(List<pbpost> list, Response response) {
        try {
            if (isNowInitial()) {
                doneInitialLoading();
                createHypbridAdapter(list);
                afterInitiateHyprbidAdapter();
            } else {
                getSwitcherBi().load_more_data(list);
                getSwitcherBi().setMaxPages(PBEditorialClient.totalPages(response));
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        // Log.d(TAG, e.toString());
    }


    /**
     * step 2:
     * this is the call for the loading the data stream externally
     */
    @Override
    protected void loadDataInitial() {
        try {
            onLoadMore(0, 1, 12);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onLoadMore(int retype, int currentpage, int ppg) throws ApiException {
        if (requestType == featureListFragment.LATEST) {
            interfacerequest.lateset(Config.posts_per_page, "DESC", currentpage, this);
        } else if (requestType == featureListFragment.CATE) {
            interfacerequest.category(slugtag, currentpage, Config.posts_per_page, "DESC", this);
        } else if (requestType == featureListFragment.SEARCH) {
            interfacerequest.category(slugtag, currentpage, Config.posts_per_page, "DESC", this);
        }
    }

}
