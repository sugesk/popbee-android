package com.popbee.pages.featureList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.hypebeast.sdk.Util.Connectivity;
import com.hypebeast.sdk.Util.PrettyTime;
import com.hypebeast.sdk.api.exception.ApiException;
import com.hypebeast.sdk.api.exception.NotFoundException;
import com.hypebeast.sdk.api.model.popbees.pbpost;
import com.hypebeast.sdk.api.resources.pb.pbPost;
import com.hypebeast.sdk.clients.PBEditorialClient;
import com.popbee.Dialog.ErrorMessage;
import com.popbee.R;
import com.popbee.life.Ad;
import com.popbee.life.Config;
import com.popbee.life.EBus;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.util.List;

import retrofit.client.Response;

/**
 * Created by hesk on 9/7/15.
 */
public class CommonListTemplate extends featureListFragment<pbpost> {

    public static CommonListTemplate B(final Bundle b) {
        final CommonListTemplate t = new CommonListTemplate();
        t.setArguments(b);
        return t;
    }

    protected pbPost interfacerequest;
    protected LifeCycleApp app;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (LifeCycleApp) activity.getApplication();
        interfacerequest = app.popbeeInterface();
    }

    @Override
    protected boolean is_list_with_ad_enabled() {
        return true;
    }

    @Override
    protected void binddata(binder holder, final pbpost data, int position) {
        try {
            final boolean f = Connectivity.isConnectedFast(getActivity());
            final String image_location = f ? data.FImage.image.msizes.med.imageURL : data.FImage.image.msizes.thumb.imageURL;

            picasso.load(image_location).error(R.drawable.popbeelogo).into(holder.big_image_single);
            holder.tvtime.setText(PrettyTime.getMoment(PBEditorialClient.DATE_FORMAT, data.mdate));

            /**
             * trigger point to the new page.
             */
            holder.click_detection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickItem(data.id);
                }
            });
            holder.tvtitle.setText(data.title);
            PBUtil.stylePBCategories(getActivity(), holder.cate_name, data.terms.cates.get(0).name);


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Successful HTTP response.
     *
     * @param list     the normnal return
     * @param response the response in object
     */
    @Override
    public void success(List<pbpost> list, Response response) {
        try {
            if (isNowInitial()) {
               /*  if (isRefresh()) {
                    EventBus.getInstance().post(new EventBus.refresh(EventBus.EVENT_SUCCESS));
                }*/
                doneInitialLoading();
                createHypbridAdapter(list);
                afterInitiateHyprbidAdapter();
                if (list.size() == 0) {
                    throw new NotFoundException("no result is found");
                } else {

                }
            } else {
                getSwitcherBi().load_more_data(list);
                getSwitcherBi().setMaxPages(PBEditorialClient.totalPages(response));
            }
        } catch (NotFoundException e) {
            ErrorMessage.alert("There is nothing found", getFragmentManager());
        } catch (Exception e) {
            ErrorMessage.alert(e.getMessage(), getChildFragmentManager());
        }
        // Log.d(TAG, e.toString());
    }

    protected void createHypbridAdapter(List<pbpost> source) {
        final featureListFragment.cateadapter noad = new featureListFragment.cateadapter(source);
        final featureListFragment.admobadp lolpp = new featureListFragment.admobadp(Ad.newAdViewTrending(getActivity()), false, Ad.AD_INTERVAL, source, lsi);
        if (source.size() < page_per_limit) {
            listview_layout.disableLoadmore();
        }
        sw = new patchHyprbid(listview_layout, noad, lolpp);
    }


    /**
     * step 2:
     * this is the call for the loading the data stream externally
     */
    @Override
    protected void loadDataInitial() {
        try {
            onLoadMore(0, 1, page_per_limit);
        } catch (ApiException e) {
            ErrorMessage.alert(e.getMessage(), getChildFragmentManager());
        }
    }

    @Override
    protected void onLoadMore(int viewType, int currentpage, int ppg) throws ApiException {
        if (requestType == featureListFragment.LATEST) {
            interfacerequest.lateset(ppg, "DESC", currentpage, this);
        } else if (requestType == featureListFragment.CATE) {
            interfacerequest.category(slugtag, currentpage, ppg, "DESC", this);
        } else if (requestType == featureListFragment.SEARCH) {
            triggerSearch(getArguments().getString(SEARCH_WORD));
        }
    }

    public void triggerSearch(final String searchWord) {
        try {
            requestType = featureListFragment.SEARCH;
            slugtag = searchWord;
            setRefreshInitial();
            interfacerequest.search(searchWord, 1, Config.posts_per_page, "DESC", this);
        } catch (Exception e) {
            ErrorMessage.alert(e.getMessage(), getChildFragmentManager());
        }
    }

    @Subscribe
    public void eventRefresh(EBus.refresh re) {
        if (re.isEventRequestStarted()) {
            setRefreshInitial();
        }
    }

    /**
     * Called when the Fragment is visible to the user.  This is generally
     * tied to {@link Activity#onStart() Activity.onStart} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStart() {
        super.onStart();
        EBus.getInstance().register(this);
    }

    /**
     * Called when the Fragment is no longer started.  This is generally
     * tied to {@link Activity#onStop() Activity.onStop} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        super.onStop();
        EBus.getInstance().unregister(this);
    }


}
