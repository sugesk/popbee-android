package com.popbee.pages.detail;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.hkm.ezwebview.Util.Fx9C;
import com.hkm.ezwebview.Util.In32;
import com.hkm.ezwebview.webviewclients.HClient;
import com.hkm.ezwebview.webviewleakfix.NonLeakingWebView;
import com.hkm.slider.Indicators.PagerIndicator;
import com.hkm.slider.SliderLayout;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.R;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;
import com.squareup.picasso.Picasso;

import io.realm.internal.Util;


/**
 * Created by hesk on 7/5/2015.
 */
public abstract class detailbse extends Fragment {
    private String url;
    public static String SURL = "theurl", TAG = "article_tag", TITLEARTICLE = "title_tag";
    private TextView line1, line2, line3, block_tv;
    private SliderLayout mslider;
    private ProgressBar mprogressbar;
    private CircleProgressBar video_loading_circle;
    private ImageView single_static_feature_image_holder;
    private NonLeakingWebView mVideo, block;
    private Picasso pica;
    private PagerIndicator pagerIndicator;
    private RelativeLayout sliderframe, video_frameview, content_article_frame;
    private ScrollView sv;
    //protected PaneloHandler _panelamo;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(singleArticle(), container, false);
    }

    public void alternative_layout_portrait_slider() {
        try {
            //todo: we need to wait until the slider has the function to support change in Slider View height.
            // RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) sliderframe.getLayoutParams();
           /* DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
            double density_pixel = dm.density * 500;
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, (int) density_pixel);
            sliderframe.setLayoutParams(params);
            sliderframe.requestLayout();*/
            // sliderframe.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract int singleArticle();

    protected abstract void presetGallery(final SliderLayout mslide);

    @SuppressLint("ResourceAsColor")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    protected void initBinding(View v) {
        line1 = (TextView) v.findViewById(R.id.article_title);
        line2 = (TextView) v.findViewById(R.id.subline_left);
        line3 = (TextView) v.findViewById(R.id.subline_right);
        block = (NonLeakingWebView) v.findViewById(R.id.content_block);
        block_tv = (TextView) v.findViewById(R.id.content_block_text);
        mslider = (SliderLayout) v.findViewById(R.id.slider_layout_framer_now);
        presetGallery(mslider);
        //  pagerIndicator = (PagerIndicator) findViewById(R.id.custom_indicator);
        mprogressbar = (ProgressBar) v.findViewById(R.id.progressc);
        mVideo = (NonLeakingWebView) v.findViewById(R.id.videoplayer);
        single_static_feature_image_holder = (ImageView) v.findViewById(R.id.image_view);
        video_frameview = (RelativeLayout) v.findViewById(R.id.framevideoplayer);
        sliderframe = (RelativeLayout) v.findViewById(R.id.sliderFrame);
        content_article_frame = (RelativeLayout) v.findViewById(R.id.content_article_frame);
        video_loading_circle = (CircleProgressBar) v.findViewById(R.id.progressloadingbarpx);
        sv = (ScrollView) v.findViewById(R.id.scroller_container);
        pica = Picasso.with(getActivity());
        extraBinding(v);

    }

    abstract void extraBinding(final View mView);

    public void reInitialize() {
        PBUtil.hideSlider(sliderframe);
        single_static_feature_image_holder.setVisibility(View.GONE);
    }

    protected relatedpost newRelatedPost() {
        final relatedpost r = new relatedpost();
        final Bundle b = new Bundle();
        b.putString("url", "related_post");
        r.setArguments(b);
        return r;
    }

    protected SliderLayout getSlid() {
        return mslider;
    }

    protected RelativeLayout getSlidFrame() {
        return sliderframe;
    }


    protected void hidevideo() {
        if (PBUtil.hideSlider(video_frameview)) {
            mVideo.loadDataWithBaseURL("", "", "text/html; charset=utf-8", "UTF-8", null);
        }
    }


    private final HClient.Callback callbackcontent = new HClient.Callback() {
        @Override
        public void retrieveCookie(String s) {

        }

        @Override
        public boolean overridedefaultlogic(String s, Activity activity) {
            return false;
        }
    };
    private final HClient.Callback callbackvideo = new HClient.Callback() {

        @Override
        public void retrieveCookie(String s) {

        }

        @Override
        public boolean overridedefaultlogic(String s, Activity activity) {
            return false;
        }
    };

    private void setup_content_block_wb(final String contentcode) {
        try {
            Fx9C.setup_content_block_wb(this, content_article_frame, block, contentcode, new Runnable() {
                @Override
                public void run() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setup_static_image(final String url_image) {
        try {
            single_static_feature_image_holder.setVisibility(View.VISIBLE);
            pica.load(url_image).error(R.drawable.popbeelogo).into(single_static_feature_image_holder);
        } catch (Exception e) {
            single_static_feature_image_holder.setVisibility(View.GONE);
        }
    }

    protected void video_webview(final String embeded_code) {
        try {
            Fx9C.setup_web_video(this, video_frameview, mVideo, video_loading_circle, embeded_code, callbackvideo, new Runnable() {
                @Override
                public void run() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void setup_infox(final String title, final String cate, final String author, final String content_block) {
        try {
            line1.setText(title);
            line2.setText(cate);
            line3.setText(author);
            setup_content_block_wb(content_block);
            //updateNumberOnCommentIcon();
        } catch (Exception e) {
            Log.d(TAG, "pb parse content:" + e.getMessage());
        }
    }


    public void isPortrait(boolean mmisPortrait) {
        final float scale = getActivity().getResources().getDisplayMetrics().density;
        int pixels = (int) (200 * scale + 0.5f);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) sliderframe.getLayoutParams();
        int landscape = (int) getResources().getDimension(R.dimen.item_height);
        int finalpixels = mmisPortrait ? -1 : landscape;
        sliderframe.setVisibility(View.INVISIBLE);
        RelativeLayout.LayoutParams rel_btn_height = new RelativeLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT, finalpixels);

        sliderframe.setLayoutParams(params);
        sliderframe.setVisibility(View.GONE);
    }

    protected void completeloading() {
        ViewCompat.animate(mprogressbar).alpha(0f).withEndAction(new Runnable() {
            @Override
            public void run() {
                mprogressbar.setVisibility(View.GONE);
            }
        });
    }

    public void scrolltoTopSmoothly() {
        sv.canScrollVertically(0);
    }


    public void killvideo() {
        try {
            hidevideo();
            Fx9C.killWebView(mVideo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
