package com.popbee.pages.detail;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.hypebeast.sdk.Util.Connectivity;
import com.hypebeast.sdk.Util.PrettyTime;
import com.hypebeast.sdk.api.model.popbees.pbpost;

import com.popbee.life.LifeCycleApp;

/**
 * Created by hesk on 9/7/15.
 */
public class relatedpost extends extrapostbase<pbpost> {
    private LifeCycleApp app;
    private relatedpostcallback cb;

    public interface relatedpostcallback {
        void handleclickonPID(final long pid);
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(Bundle)} will be called after this.
     *
     * @param activity the activity is here
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (LifeCycleApp) activity.getApplication();
        cb = (relatedpostcallback) activity;
    }

    /**
     * the display item limited
     *
     * @return the integer inside
     */
    @Override
    protected int getDisplayItemLimit() {
        return 4;
    }

    /**
     * get data by its data type
     *
     * @param pbpost the type of the data
     * @return the string
     */
    @Override
    protected String getPostImageURL(pbpost pbpost) {
        final boolean f = Connectivity.isConnectedFast(getActivity());
        final String i = f ? pbpost.FImage.image.msizes.med.imageURL : pbpost.FImage.image.msizes.thumb.imageURL;
        return i;
    }

    /**
     * get data by its data type
     *
     * @param mpbpost the type of the data
     * @return the string
     */
    @Override
    protected String getSingleEnd(pbpost mpbpost) {
        return "";
    }

    /**
     * just to find the PID of the post
     *
     * @param mpbpost from the url
     * @return the long for PID
     */
    @Override
    protected long getSinglePID(pbpost mpbpost) {
        return mpbpost.id;
    }


    /**
     * get data by its data type
     *
     * @param pbpost the type of the data
     * @return the string
     */
    @Override
    protected String getTextTitle(pbpost pbpost) {
        return pbpost.title;
    }

    /**
     * get data by its data type
     *
     * @param p the type of the data
     * @return the string
     */
    @Override
    protected String getTextCate(pbpost p) {
        final String cateName = p.terms.cates.get(0).name;
        final String mtitle = p.title;
        return cateName == null ? mtitle : cateName;
    }

    /**
     * get data by its data type
     *
     * @param pbpost the type of the data
     * @return the string
     */
    @Override
    protected String getTextTime(pbpost pbpost) {
        return PrettyTime.getDDMMYYY(pbpost.mmodified);
    }

    @Override
    protected void onClickItem(String route) {
        //  PBUtil.routeSinglePage(route, getActivity());
        Log.d(TAG, route);
    }

    @Override
    protected void onClickItem(long pid) {
        cb.handleclickonPID(pid);
    }

    /**
     * step 2:
     * this is the call for the loading the data stream externally
     *
     * @param confirmAdapter extrapostbase object
     */
    @Override
    protected void loadDataInitial(extrapostbase.ccAdp confirmAdapter) {
        if (app.getMainList().size() == 0) return;
        for (int i = 0; i < getDisplayItemLimit(); i++) {
            confirmAdapter.insert(app.getMainList().get(i));
        }
    }


}
