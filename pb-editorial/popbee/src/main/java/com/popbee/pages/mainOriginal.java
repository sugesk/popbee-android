package com.popbee.pages;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neopixl.pixlui.components.textview.TextView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v13.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v13.FragmentPagerItems;
import com.popbee.R;
import com.popbee.patch.FixedSpeedScroller;
import com.popbee.patch.pathInterLayout;

import java.lang.reflect.Field;

/**
 * Created by hesk on 3/7/15.
 */
public class mainOriginal extends Fragment {
    public static String TAG = "mainOriginal";
    private SmartTabLayout mTab;
    private ViewPager pager;
    private FragmentManager mChildFragmentManager;
    private TextView titleTabTextView;
    private FragmentPagerItemAdapter mfragmentadapter;
    protected pathInterLayout pathFix;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_design_viewpager, container, false);
    }

    public FragmentPagerItemAdapter newAdapter() {
        FragmentPagerItemAdapter adp = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                /**
                 * adding the fragment of the list template into the tabs
                 */
                .add(tB.latest.getStringId(), tB.latest.getClazz(), tB.latest.bundle())
                .add(tB.trending.getStringId(), tB.trending.getClazz(), tB.trending.bundle())
                .create());
        return adp;
    }

    protected void pagerChanged(int position) {

    }

    @SuppressLint("ResourceAsColor")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(View v, Bundle b) {
        mTab = (SmartTabLayout) v.findViewById(R.id.materialTabHost);
        pager = (ViewPager) v.findViewById(R.id.viewpager);
        titleTabTextView = (TextView) v.findViewById(R.id.titleTab);
        pager.setAdapter(newAdapter());
        pager.setOffscreenPageLimit(2);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                pagerChanged(position);
            }

            @Override
            public void onPageSelected(int position) {
                pagerChanged(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTab.setViewPager(pager);
        FixedSpeedScroller.setSmoothScroller(pager, getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
