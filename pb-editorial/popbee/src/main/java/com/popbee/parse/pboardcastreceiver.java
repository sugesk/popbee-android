package com.popbee.parse;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.popbee.Home;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hesk on 10/8/15.
 */
public class pboardcastreceiver extends ParsePushBroadcastReceiver {

    private static final String notification_name = "com.popbee.NOTIFICATION";
    private static final String TAG = "BR_pboardcastreceiver";
    private static final int NOTIFICATION_ID = 1;
    private static int numMessages = 0;

    private NotificationUtils notificationUtils;
    private Intent parseIntent;

    /**
     * Push Receiver
     *
     * @param context the context bar
     * @param intent  the intent implementation
     */
    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.e(TAG, "Push received: " + json);
            parseIntent = intent;
            parsePushJson(context, json);
        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    protected Class getActivity(Context context, Intent intent) {
        return Home.class;
    }

    /**
     * Parses the push notification json
     *
     * @param context the context
     * @param json    json text
     */
    private void parsePushJson(Context context, JSONObject json) {
        try {
            boolean isBackground = json.getBoolean("is_background");
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String message = data.getString("message");
            if (!isBackground) {
                Intent resultIntent = new Intent(context, Home.class);
                showNotificationMessage(context, title, message, resultIntent);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }


    /**
     * Shows the notification message in the notification bar
     * If the app is in background, launches the app
     *
     * @param context context
     * @param title   the title
     * @param message message in the notification
     * @param intent  the intent from there
     */
    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.putExtras(parseIntent.getExtras());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent);
    }
}
