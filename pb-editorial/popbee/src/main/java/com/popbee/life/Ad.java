package com.popbee.life;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;

import com.facebook.ads.AdSettings;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.marshalchen.ultimaterecyclerview.ui.AdGoogleDisplaySupport;
import com.popbee.BuildConfig;

/**
 * Created by hesk on 22/12/15.
 */
public class Ad {
    public static final int AD_INTERVAL = 7;
    public static final double RATIO = 1.2d;
    public static final double RATIO_SCALE_LIST_AD = 1.2d;

    public static void newBannerView(RelativeLayout container) {
        DisplayMetrics dm = container.getContext().getResources().getDisplayMetrics();
        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);
        AdSize adSize = AdSize.BANNER;
        final AdView mAdView = new AdView(container.getContext());
        mAdView.setAdSize(adSize);
        mAdView.setAdUnitId(BuildConfig.DFP_INPOST_ID);

        // Create an ad request.
        AdRequest.Builder mRequestBuilder = new AdRequest.Builder();
        mRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        mAdView.loadAd(mRequestBuilder.build());
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                int h = mAdView.getLayoutParams().height;
                AdGoogleDisplaySupport.scale(mAdView, RATIO);
                AdGoogleDisplaySupport.panelAdjust(mAdView, (int) (h * RATIO));
            }
        });
        container.addView(mAdView);
    }

    public static RelativeLayout newAdViewTrending(Activity activity) {
        return adview_custom_made(activity, AdSize.MEDIUM_RECTANGLE, BuildConfig.DFP_LIST_UNIT_ID);
    }

    private static RelativeLayout adview_custom_made(Activity activity, AdSize _size, String AdUnitId) {
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        final AdView mAdView = new AdView(activity);
        mAdView.setAdSize(_size);
        mAdView.setAdUnitId(AdUnitId);
        // Create an ad request.
        AdRequest.Builder mRequestBuilder = new AdRequest.Builder();
        mRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        // Start loading the ad.
        mAdView.loadAd(mRequestBuilder.build());
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        final RelativeLayout layout = AdGoogleDisplaySupport.initialSupport(activity, mDisplayMetrics);
        final int ad_height = AdGoogleDisplaySupport.defaultHeight(mDisplayMetrics);
        AdGoogleDisplaySupport.panelAdjust(mAdView, (int) (ad_height * RATIO_SCALE_LIST_AD));
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                int h = mAdView.getLayoutParams().height;
                AdGoogleDisplaySupport.scale(mAdView, RATIO_SCALE_LIST_AD);
                AdGoogleDisplaySupport.panelAdjust(mAdView, (int) (h * RATIO_SCALE_LIST_AD));
                //  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            }
        });
        layout.addView(mAdView);
        return layout;
    }

}
