package com.popbee.life;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.squareup.otto.Bus;

/**
 * Created by hesk on 14/7/15.
 */
public class EBus {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private EBus() {

    }

    public static class menuclass {
        private String tag, title;
        private String[] st;

        public menuclass(String[] tag, String title) {
            this.st = tag;
            crunch();
            this.title = title;
        }

        private void crunch() {
            if (st.length == 3) {
                tag = st[2];
            } else if (st.length == 2) {
                tag = st[1];
            } else {
                tag = st[0];
            }
        }

        public String gettitle() {
            return title;
        }

        public String getslug() {
            return tag;
        }

    }




    public static class Scrolling {
        public static boolean touchSync;
        private boolean enabled;
        private boolean directionUp;

        public Scrolling(boolean statusEnabled, boolean directionUp) {
            this.enabled = statusEnabled;
            this.directionUp = directionUp;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

        public boolean getDirectionUpFreeHand() {
            return this.directionUp;
        }

        public static class ScrollEvent extends RecyclerView.OnScrollListener {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager lmg = (LinearLayoutManager) recyclerView.getLayoutManager();
                    final boolean F1PosA32 = lmg.findFirstCompletelyVisibleItemPosition() == 0;
                    EBus.getInstance().post(new EBus.Scrolling(F1PosA32, dy < 0 && !Scrolling.touchSync));
                }

            }
        }

        public static class SyncTouchEvent implements View.OnTouchListener {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN) {
                    Scrolling.touchSync = true;
                } else {
                    Scrolling.touchSync = false;
                }
                return false;
            }
        }

    }

    public static final int EVENT_REQUEST = 12;
    public static final int EVENT_SUCCESS = 13;

    public static class refresh {
        private int event;

        public refresh(int event_start) {
            this.event = event_start;
        }

        public boolean isEventRequestStarted() {
            return event == EVENT_REQUEST;
        }

        public boolean isEventReturnSuccess() {
            return event == EVENT_SUCCESS;
        }
    }
}
