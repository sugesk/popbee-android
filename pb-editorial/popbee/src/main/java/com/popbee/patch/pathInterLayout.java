package com.popbee.patch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by hesk on 14/8/15.
 */
public class pathInterLayout extends RelativeLayout {
    public pathInterLayout(Context context) {
        super(context);
    }

    private boolean touchable_eat = false;

    public pathInterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public pathInterLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return touchable_eat;
    }

    public void setTouchableEat(boolean h) {
        touchable_eat = h;
    }

}
