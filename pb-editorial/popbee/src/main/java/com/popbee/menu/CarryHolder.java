package com.popbee.menu;

import android.view.View;

import com.hkm.slidingmenulib.advancedtreeview.SmartItem;
import com.hkm.slidingmenulib.advancedtreeview.presnt.slack.Parent;
import com.neopixl.pixlui.components.textview.TextView;

/**
 * Created by hesk on 14/7/15.
 */
public class CarryHolder extends Parent<SmartItem, TextView> {

    public CarryHolder(View itemView) {
        super(itemView);
        setNotifcationFieldEnabled(false);
        forceTitleCapitalized(true);
    }


}
