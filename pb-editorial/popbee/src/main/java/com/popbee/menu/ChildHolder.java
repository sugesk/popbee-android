package com.popbee.menu;

import android.view.View;

import com.hkm.slidingmenulib.advancedtreeview.SmartItem;
import com.hkm.slidingmenulib.advancedtreeview.presnt.slack.Child;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.life.EBus;

/**
 * Created by hesk on 14/7/15.
 */
public class ChildHolder extends Child<SmartItem, TextView> {
    public ChildHolder(View itemView) {
        super(itemView);
        forceTitleCapitalized(true);
    }

    @Override
    protected void request_api(String[] n, String title) {
        EBus.getInstance().post(new EBus.menuclass(n, title));
    }
}
