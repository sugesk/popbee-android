package com.popbee;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.hkm.advancedtoolbar.Util.ErrorMessage;
import com.hkm.advancedtoolbar.V5.BeastBar;
import com.hkm.slidingmenulib.layoutdesigns.singleDetailPost;
import com.hypebeast.sdk.Util.Connectivity;
import com.hypebeast.sdk.api.exception.ApiException;
import com.hypebeast.sdk.api.exception.NotFoundException;
import com.hypebeast.sdk.api.model.popbees.pbpost;
import com.hypebeast.sdk.api.resources.pb.pbPost;
import com.hypebeast.sdk.application.popbee.pbPostSearch;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;
import com.popbee.pages.detail.detail;
import com.popbee.pages.detail.relatedpost;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrListener;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hesk on 7/5/2015.
 */
public class Single extends singleDetailPost<detail> implements Callback<pbpost>, detail.loadData, SlidrListener, relatedpost.relatedpostcallback {
    private pbPost pb;

    private detail detailf;
    private BeastBar actionToolBar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    @Override
    protected void configToolBar(final Toolbar tb) {
        BeastBar.Builder bb = new BeastBar.Builder();
        bb.back(R.mipmap.ic_action_arrow_left);
        bb.background(R.drawable.actionbar_bg_white_greyline);
        bb.companyIcon(R.drawable.actionbar_bg_pb_logo);
        bb.setToolBarTitleSize(R.dimen.toolbar_title);
        bb.setToolBarTitleColor(R.color.main_background);
        // bb.setFontFace(this, "FrancoisOne.ttf");
        actionToolBar = BeastBar.withToolbar(this, tb, bb);
        actionToolBar.setBackIconFunc(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
        Slidr.attach(this, PBUtil.getSlidrConfig(getApplication(), this));
    }

    @Override
    protected void loadPageWithFullURL(String url) {

    }

    public void onEvent(long pid_post) {
        loadPageWithPID(pid_post);
    }

    @Override
    protected void loadPageWithPID(final long pid) {
        try {
            pbpost p = pbPostSearch.findById(pid, ((LifeCycleApp) getApplication()).getMainList());
            success(p, null);
        } catch (NotFoundException e) {
            try {
                pb.the_post(pid, this);
            } catch (ApiException eb) {
                eb.printStackTrace();
            }
        }
    }


    @Override
    protected detail getInitFragment() throws Exception {
        pb = ((LifeCycleApp) getApplication()).popbeeInterface();
        //initiation of a child fragment
        detailf = new detail();
        detailf.setLoader(this);
        //starting to see the child fragment now
        return detailf;
    }

    @Override
    protected void onMenuItemSelected(@IdRes int Id) {

    }

    private pbpost final_post_item;

    /**
     * Successful HTTP response.
     *
     * @param postfinal the normnal return
     * @param response  the response in object
     */
    @Override
    public void success(final pbpost postfinal, final Response response) {
        final boolean k = Connectivity.isConnectedFast(this);
        try {
            String cateName = postfinal.terms.cates.get(0).name;
            final String mtitle = postfinal.title;
            cateName = cateName == null ? mtitle : cateName;
            actionToolBar.setActionTitle(cateName);
            detailf.setup_content(
                    mtitle,
                    cateName,
                    PBUtil.pbDate(postfinal.mmodified) + " / " + postfinal.author.nickname,
                    postfinal.htmlcontent);
            detailf.setShareContentBar(mtitle, postfinal.excerpt, postfinal.finalenpoint);
            if (postfinal.popbee_specific.custom_post_template.equalsIgnoreCase("single-gallery-portrait.php")) {
                detailf.alternative_layout_portrait_slider();
                if (postfinal.popbee_specific.gallery.size() > 1) {
                    detailf.setup_gallery(postfinal.popbee_specific.gallery);
                } else if (postfinal.popbee_specific.gallery.size() == 1) {
                    detailf.setup_single_im(postfinal.popbee_specific.gallery.get(0));
                    detailf.closeSlider();
                }
                detailf.closeVideo();
            } else if (postfinal.popbee_specific.gallery != null) {
                //   detailf.isPortrait(false);
                if (postfinal.popbee_specific.gallery.size() > 1) {
                    detailf.setup_gallery(postfinal.popbee_specific.gallery);
                } else if (postfinal.popbee_specific.gallery.size() == 1) {
                    detailf.setup_single_im(postfinal.popbee_specific.gallery.get(0));
                    detailf.closeSlider();
                }
                detailf.closeVideo();
            } else if (postfinal.popbee_specific.custom_post_template.equalsIgnoreCase("single-video.php")) {
                if (k) {
                    detailf.setup_video(postfinal.popbee_specific.video_embeded_src);
                    detailf.closeSlider();
                }
            } else {
                detailf.setup_single_im(k ? postfinal.FImage.image.msizes.med.imageURL : postfinal.FImage.image.msizes.thumb.imageURL);
                detailf.closeSlider();
                detailf.closeVideo();
            }

            detailf.complete();
            // Toast.makeText(this, "data is up and done " + postfinal.finalenpoint, Toast.LENGTH_LONG);
            PBUtil.GoogleTrackerScreen(this, postfinal.finalenpoint);
            final_post_item = postfinal;
        } catch (Exception e) {
            ErrorMessage.alert("error:" + e.getLocalizedMessage(), getFragmentManager(), new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        }
    }

    /**
     * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
     * exception.
     *
     * @param error the error object
     */
    @Override
    public void failure(RetrofitError error) {
        ErrorMessage.alert("retro error:" + error.getLocalizedMessage(), getFragmentManager(), new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    @Override
    public void onLoad() {
        startIntentArgument();
    }

    @Override
    public void onShareClick() {
        if (final_post_item != null) {
            PBUtil.easyCallShareProvoider(this, final_post_item.excerpt, final_post_item.finalenpoint);
        }
    }

    @Override
    public void onCommentClick() {
        if (final_post_item != null) {
            PBUtil.easyFBCommentary(this, final_post_item.finalenpoint);
        }
    }

    @Override
    public void onSlideStateChanged(int i) {

    }

    @Override
    public void onSlideChange(float v) {

    }

    @Override
    public void onSlideOpened() {

    }

    @Override
    public void onSlideClosed() {

    }

    @Override
    public void finish() {
        if (detailf != null)
            detailf.killvideo();
        super.finish();
    }


    @Override
    protected void onPause() {
        if (detailf != null)
            detailf.killvideo();
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void handleclickonPID(long pid) {
        detailf.reInitialize();
        //todo: this is not working..
        detailf.scrolltoTopSmoothly();
        loadPageWithPID(pid);
    }

    @Override
    protected int getDefaultMainActivityLayoutId() {
        return R.layout.custom_single;
    }

}
