# Add project specific ProGuard rules here.

-keep class org.simple.** { *; }
-keep interface org.simple.** { *; }
-keepclassmembers class * { @org.simple.eventbus.Subscriber <methods>; }
-keepclassmembers class ** { public void onEvent*(**); }
 # Only required if you use AsyncExecutor
-keepclassmembers class * extends de.greenrobot.event.util.ThrowableFailureEvent {
      <init>(java.lang.Throwable);
}
-keepclassmembers, allowobfuscation class ** {
    @de.halfbit.tinybus.Subscribe public *;
    @de.halfbit.tinybus.Produce public *;
}
-keepnames public class * extends io.realm.RealmObject
-keep class io.realm.** { *; }
-dontwarn javax.**
-dontwarn io.realm.**
-keep class !android.support.v7.internal.view.menu.**,android.support.** {*;}
-keep class !android.support.v7.internal.widget.**,android.support.** {*;}
-keep public class com.google.android.gms.ads.** { public *; }
-keep public class com.google.ads.** { public *; }
-keep public class com.google.ads.mediation.** { public *; }